#### Project FrozenFist
### VortexOps
  
  ![Project Image](https://image.ibb.co/grYLoy/Frozen_Fist.png)
  
  Software for semi-autonomous armored assault vehicles. The vehicles are
  designed to operate either remotely with a human pilot or in 
  "robot mode" with guidance from an on-board Artificial Intelligence (AI) system.
  
  **Contact** [Wendy McDonald](wendy.mcdonald@smail.rasmussen.edu)
  
### Pushed to Bitbucket Repository - May 18, 2018
### Modified README on Bitbucket - May 24, 2018