#####Assult Vehicle Robot Mode
###Basic actions of Drone

* Drone will search for nearest target
  *If* target is **Non-Friendly** *and* **not obstructed** drone will determine if target is in range 
  *If* target is in range drone will fire at its best determined interval
  
* Drone will determine if target is down
  *If* target is not down drone will fire another round of shots
  *If* target is down and **vehicle is still operational** drone will begin searching for new target
  
* Drone will search for additional targets
  *If* more targets exist it will begin targeting steps over again
  *If* no targets remain bonus points will be assigned and a [victory song will play](https://www.youtube.com/watch?v=04854XqcfCY)
  