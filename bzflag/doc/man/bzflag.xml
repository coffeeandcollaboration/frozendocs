<?xml version="1.0" encoding="ISO-8859-1"?>
<!DOCTYPE refentry PUBLIC "-//OASIS//DTD DocBook XML V4.1.2//EN"
                   "http://www.oasis-open.org/docbook/xml/4.1.2/docbookx.dtd">
<!-- lifted from troff+man by doclifter -->
<refentry id='bzflag6'>
<refmeta>
<refentrytitle>bzflag</refentrytitle>
<manvolnum>6</manvolnum>
</refmeta>
<refnamediv id='name'>
<refname>BZFlag</refname>
<refpurpose>a tank battle game</refpurpose>
</refnamediv>
<!-- body begins here -->
<refsynopsisdiv id='synopsis'>
<cmdsynopsis>
  <command>bzflag</command>

    <arg choice='opt'>-3dfx </arg>
    <arg choice='opt'>-no3dfx </arg>
    <arg choice='opt'>-anonymous </arg>
    <arg choice='opt'>-directory <replaceable>directory</replaceable></arg>
    <arg choice='opt'>-echo </arg>
    <arg choice='opt'>-echoClean </arg>
    <arg choice='opt'><arg choice='plain'>-geometry <replaceable>widthxheight</replaceable></arg><arg choice='req'><group choice='opt'><arg choice='plain'>+ </arg><arg choice='plain'>- </arg></group><arg choice='plain'><replaceable>x</replaceable></arg><group choice='opt'><arg choice='plain'>+ </arg><arg choice='plain'>- </arg></group><arg choice='plain'><replaceable>y</replaceable></arg></arg></arg>
    <arg choice='opt'>-latitude <replaceable>latitude</replaceable></arg>
    <arg choice='opt'>-list <replaceable>url</replaceable></arg>
    <arg choice='opt'>-locale <replaceable>locale</replaceable></arg>
    <arg choice='opt'>-longitude <replaceable>longitude</replaceable></arg>
    <arg choice='opt'>-multisample </arg>
    <arg choice='opt'>-mute </arg>
    <arg choice='opt'>-nolist </arg>
    <arg choice='opt'>-solo <replaceable>number-of-robots</replaceable></arg>
    <arg choice='opt'><arg choice='plain'>-team </arg><group choice='opt'><arg choice='plain'><replaceable>automatic</replaceable></arg><arg choice='plain'><replaceable>red</replaceable></arg><arg choice='plain'><replaceable>green</replaceable></arg><arg choice='plain'><replaceable>blue</replaceable></arg><arg choice='plain'><replaceable>purple</replaceable></arg><arg choice='plain'><replaceable>rogue</replaceable></arg></group></arg>
    <arg choice='opt'>-version </arg>
    <arg choice='opt'><arg choice='plain'>-view </arg><group choice='opt'><arg choice='plain'><replaceable>normal</replaceable></arg><arg choice='plain'><replaceable>stereo</replaceable></arg><arg choice='plain'><replaceable>three</replaceable></arg></group></arg>
    <arg choice='opt'>-window </arg>
    <arg choice='opt'><arg choice='plain'>-zbuffer </arg><group choice='opt'><arg choice='plain'><replaceable>on</replaceable></arg><arg choice='plain'><replaceable>off</replaceable></arg></group></arg>
    <arg choice='opt'><arg choice='opt'><replaceable>callsign@</replaceable></arg><arg choice='plain'><replaceable>server</replaceable></arg><arg choice='opt'><replaceable>:port</replaceable></arg></arg>
</cmdsynopsis>
</refsynopsisdiv>


<refsect1 id='description'><title>DESCRIPTION</title>
<para><emphasis remap='B'>BZFlag</emphasis>
is a 3D multi-player tank battle game that allows users
to play against each other in a networked environment.
There are five teams:  red, green, blue, purple and rogue
(rogue tanks are black).  Destroying a player on another
team scores a win, while being destroyed or destroying a
teammate scores a loss.  Rogues have no teammates (not even
other rogues), so they cannot shoot teammates and they do
not have a team score.</para>

<para>There are three main styles of play:  capture-the-flag,
free-for-all, and rabbit-hunt.  In capture-the-flag, each team (except rogues)
has a team base and each team with at least one player has a team
flag.  The object is to capture an enemy team's flag by bringing
it to your team's base.  This destroys every player on the captured
team, subtracts one from that team's score, and adds one to your
team's score.  In free-for-all, there are no team flags or team
bases.  The object is simply to get as high a score as possible.
In rabbit-hunt, the lead player is chosen as the target for all
other players. When the rabbit(target) is destroyed, the live player
with the next highest score becomes the rabbit. The object is to
remain the rabbit for as long as possible. The rabbit is marked as
a white tank.</para>

<refsect2 id='options'><title>Options</title>
<!-- .RS -->
<variablelist remap=".TP">
<varlistentry>
<term><option>-3dfx</option></term>
<listitem>
<para>For Mesa users with a passthrough 3Dfx card, e.g. a Voodoo or
Voodoo 2 based card.  This sets the MESA_GLX_FX environment
variable to use fullscreen passthrough mode.  Use <option>-geometry</option>
to use a resolution other than 640x480 on the passthrough card.
You should not use this option with <option>-window</option>.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-no3dfx</option></term>
<listitem>
<para>For Mesa users with a passthrough 3Dfx card, such as Voodoo or
Voodoo 2 based cards.  This unsets the MESA_GLX_FX environment
variable so that the passthrough card isn't used.  Use this
option if MESA_GLX_FX is normally set in your environment and
you don't want bzflag to use the passthrough card.  This option
is only a convenience;  you can achieve the same effect by
unsetting MESA_GLX_FX in your environment.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-anonymous</option></term>
<listitem>
<para>Uses the email address <emphasis remap='I'>anonymous</emphasis> instead of <emphasis remap='I'>username@hostname</emphasis>.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-directory</option><replaceable> directory</replaceable></term>
<listitem>
<para>Looks for data files in <emphasis remap='I'>directory</emphasis> first.  This defaults to a
directory named <emphasis remap='I'>data</emphasis> in the current directory.  If not found
there, the game looks for data files in the current directory, then
in the default installation location <filename>/usr/local/share/bzflag</filename>.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-echo</option></term>
<listitem>
<para>Copies all message window output to the shell.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-echoClean</option></term>
<listitem>
<para>Copies all message window output to the shell, but without the color coding.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-geometry</option><replaceable> width</replaceable><emphasis remap='B'>x</emphasis><emphasis remap='I'>height</emphasis>[{<option>+</option>|<option>-</option>}x{<option>+</option>|<option>-</option>}y]</term>
<listitem>
<para>This specifies the size and, optionally, the position of the window. It
can be used with or without the <option>-window</option> option. It may be necessary
to use this on some systems when bzflag cannot correctly determine the
display size.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-latitude</option><replaceable> latitude</replaceable></term>
<listitem>
<para>Uses <emphasis remap='I'>latitude</emphasis> when computing celestial object positions.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-list</option><replaceable> url</replaceable></term>
<listitem>
<para>Look for <emphasis remap='B'>bzfls</emphasis> servers using <emphasis remap='I'>url</emphasis>.  A built-in url is
used by default (the same url is the default for <emphasis remap='B'>bzfs</emphasis>).
See <emphasis remap='B'>bzfls</emphasis> for a description of the format of <emphasis remap='I'>url</emphasis>.
If <emphasis remap='I'>url</emphasis> is <emphasis remap='I'>default</emphasis> then the url is reset to the built-in
url (the url is remembered across invocations of <command>bzflag</command>).
<emphasis remap='B'>Bzfls</emphasis> servers keep a list of <emphasis remap='B'>bzfs</emphasis> servers accessible
from the internet and are queried when using the Find Server menu.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-locale</option><replaceable> locale</replaceable></term>
<listitem>
<para>Set the locale used to display messages, menus, hud alerts, etc.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-longitude </option><emphasis remap='I'>longitude</emphasis></term>
<listitem>
<para>Uses <emphasis remap='I'>longitude</emphasis> when computing celestial object positions.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-multisample</option></term>
<listitem>
<para>Uses a multisample buffer for rendering.  If multisampling isn't
available then the application will terminate.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-mute</option></term>
<listitem>
<para>Disables sound.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-nolist</option></term>
<listitem>
<para>Disables <emphasis remap='B'>bzfls</emphasis> server querying.  See <option>-list</option>.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-solo</option><replaceable> number-of-robots</replaceable></term>
<listitem>
<para>When you join a game, you'll also cause <emphasis remap='I'>number-of-robots</emphasis> robots
to join too.  This is an experimental option and the robots are
extremely stupid players.  Robots are added to teams at random.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-team</option><replaceable> team-name</replaceable></term>
<listitem>
<para>Chooses the player's team.  If there are no team positions available
and the team-name is set to be automatic, the player will try to
join as an observer.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-version</option></term>
<listitem>
<para>Prints the version number of the executable.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-view</option> {<emphasis remap='I'>normal</emphasis> | <emphasis remap='I'>stereo</emphasis> | <emphasis remap='I'>three</emphasis>}</term>
<listitem>
<para>Chooses one of three possible display options.  <emphasis remap='I'>Normal</emphasis> will
render a single view to the entire screen.  <emphasis remap='I'>Stereo</emphasis> will try
to allocate a stereo (in-a-window) capable buffer and then draw a
single view in stereo.  You're system must support stereo-in-a-window
buffers and stereo goggles.  <emphasis remap='I'>Three</emphasis> will render the front view
to the upper right quadrant of the display, a left view to the lower
left quadrant, and a right view to the lower right quadrant.  This
is intended for systems capable of driving multiple monitors from
various areas of the display surface, yielding a wrap around view.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-window</option></term>
<listitem>
<para>Runs the application in a window instead of full screen.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><option>-zbuffer</option> {<emphasis remap='I'>on</emphasis> | <emphasis remap='I'>off</emphasis>}</term>
<listitem>
<para>When <emphasis remap='I'>off</emphasis> is chosen the game will not attempt to allocate a
depth (z) buffer and will use a different rendering technique for
hidden surface removal.  Some systems may be capable of using a
higher screen resolution if a depth buffer isn't allocated.</para>
</listitem>
</varlistentry>
<varlistentry>
<term>[<emphasis remap='I'>callsign@</emphasis>]<emphasis remap='I'>server</emphasis>[<emphasis remap='I'>:port</emphasis>]</term>
<listitem>
<para>Specifies the callsign you want, and the host running the <emphasis remap='B'>bzfs</emphasis>
server. Multiple independent games can be run on a single network, or
even on different ports on a single computer. Which server and port you
choose decides which game you enter.
The callsign and the port is optional. If you don't specify a port the
standard server port will be used, and if you don't specify a callsign the
callsign used for the previous game is used.  If that cannot be found
then the callsign is the value of the <emphasis remap='B'>BZFLAGID</emphasis> environment variable.
If <emphasis remap='B'>BZFLAGID</emphasis> is empty or undefined then <command>bzflag</command> will prompt for a
callsign when joining a game.</para>
</listitem>
</varlistentry>
<varlistentry>
<term></term>
<listitem>
<para>&nbsp;</para> <!-- FIX-ME: empty list item -->
</listitem>
</varlistentry>
</variablelist> <!-- .TP -->
</refsect2>

<refsect2 id='controls'><title>Controls</title>
<para>Tanks are controlled by moving the mouse within the large yellow box
in the main view.  When the mouse is inside the small yellow box,
the tank is motionless.  The large box is the limit of the tank's
speed.</para>

<para>Shots are fired by pressing the
<emphasis remap='I'>left mouse button</emphasis>
The type of shot fired depends on what flag the tank has.  Normal
shots last about 3.5 seconds.  Reloading also takes 3.5 seconds for
normal shots.</para>

<para>Pressing the
<emphasis remap='I'>middle mouse button</emphasis>
drops a flag.  Nothing will happen if the tank has no flag or is
not allowed to drop the flag for some reason (e.g. it's a bad flag).
Flags are picked up by driving over them.  A dropped flag gets
tossed straight up;  it falls to the ground in about 3 seconds.</para>

<para>Pressing the
<emphasis remap='I'>right mouse button</emphasis>
identifies the closest player centered in the view.  If your tank
has the guided missile super-flag, this will also lock the missile
on target.  However, the target must be carefully centered for the
missile to lock.</para>

<para>When the server allows jumping or if the tank has the jumping flag,
the
<emphasis remap='I'>Tab</emphasis>
key jumps.  Tanks can jump onto buildings, however there is no
way to shoot downward (or upward) with a regular shot.  The guided
missile and the shock wave are two ways of destroying a tank
on or from a building.</para>

<para>The current radar range can be changed by pressing the
<emphasis remap='I'>1,</emphasis>
<emphasis remap='I'>2,</emphasis>
or
<literal>3</literal>
keys above the alphabetic keys for low, medium, and long range,
respectively.
The
<emphasis remap='I'>f</emphasis>
key toggles the flag help display, which describes the flag
in the tank's possession.  Displaying help does
<emphasis remap='B'>not</emphasis>
pause the game.</para>

<para>The
<emphasis remap='I'>Pause</emphasis>
key pauses and resumes play.  When paused, the tank cannot be
destroyed nor can it's shots destroy other players.  The reload
countdown is suspended and the radar and view are blanked when
paused.  A paused tank has a transparent black sphere surrounding
it.  Since a paused tank is invulnerable a player could cheat by
pausing just before being destroyed, so there's a brief delay before
the pause takes effect.  This delay is long enough to make pausing
effectively useless for cheating.  Pressing
<emphasis remap='I'>Pause</emphasis>
before the pause takes effect cancels the pause request.</para>

<para>The
<emphasis remap='I'>Delete</emphasis>
key initiates a self destruct sequence. You will see a countdown that
can be stopped by pressing
<emphasis remap='I'>Delete</emphasis>
once more. Otherwise, you tank will self destruct. This can be useful if
your tank gets stuck and there is no other tank around to shoot you.</para>

<para>The list of players and scores is displayed when your tank is
paused or dead.  Pressing the
<emphasis remap='I'>s</emphasis>
key toggles the score display when alive and not paused.</para>

<para>The
<emphasis remap='I'>b</emphasis>
key toggles binoculars, which gives a close up view of distant objects.
The
The
<literal>9</literal>
key toggles Otto, the automatic pilot, on and off.
<emphasis remap='I'>t</emphasis>
key toggles the frame rate display and the
<emphasis remap='I'>y</emphasis>
key toggle the frame time display. The time of day can be changed with the
<emphasis remap='I'>plus</emphasis>
and
<emphasis remap='I'>minus</emphasis>
keys, which advance and reverse the time by 5 minutes, respectively.
The time of day in the game is initialized to the system's clock.
In addition, the latitude and longitude are used to calculate the
positions of celestial objects.</para>

<para>The
<emphasis remap='I'>Esc</emphasis>
key shows the game menu.  Use the
<emphasis remap='I'>Enter</emphasis>
and arrow keys to navigate the menu and the
<emphasis remap='I'>Esc</emphasis>
key to return to the previous menu or hide the main menu.
The menus allow you to start a new server, join a game, leave a game
and enter another, change the rendering options, change the display
resolution, change the sound volume, remap the meanings of keys, browse
online help, and quit the game.</para>

<para>The display resolution is not always available for changing.  If it
is, use the
<emphasis remap='I'>t</emphasis>
key to test a selected resolution;  it will be loaded for a few
seconds and then the previous resolution restored.  Press the
<emphasis remap='I'>Enter</emphasis>
key to permanently select a new resolution. When you quit the game,
the resolution is restored to what it was before the game started.</para>

<para>Options are recorded between game sessions in the .bzflag file (or .bzflag.$(HOST)
if the <emphasis remap='I'>HOST</emphasis> environment variable is defined) in
the user's home directory.  This file has a simple name/value pair
format.  This file is completely rewritten by the game after each
session.</para>

<para>You can send typed messages to other players by pressing the
<emphasis remap='I'>m</emphasis>
or
<emphasis remap='I'>n</emphasis>
keys.  The
<emphasis remap='I'>m</emphasis>
key will send a message to your teammates only.  Rogue players cannot
send these messages.  The
<emphasis remap='I'>n</emphasis>
key will send a message to all the other players.  After pressing the
key, just type your message and press enter or Control-D.  To cancel
a message, you can enter a blank message or press Delete, Escape, or
Control-C.  Be careful with the Escape key;  pressing Escape once will
cancel the message, pressing it again will show the main menu.  Backspace
will delete the most recently typed character.  The Tab key doesn't add
a tab to the message but instead causes the tank to jump (as usual).
You can also send a direct message to a single player by pressing the
<emphasis remap='I'>,</emphasis>
or
<literal>.</literal>
keys. The
<emphasis remap='I'>,</emphasis>
key will send your message to your 'nemesis', i.e. the last player who
killed you or was killed by you. The
<literal>.</literal>
key will send a private message to another player. You can choose the
recipient by using the left and right arrow keys.
<emphasis remap='I'>o</emphasis>
toggles the quick-admin interface. Use the arrow keys to select a
command, then fill in the necessary parameters</para>
</refsect2>

<refsect2 id='scoring'><title>Scoring</title>
<para>An individual's score is the difference between that player's wins and
losses.  A win is scored for each enemy tank destroyed.  A loss is
scored for each teammate destroyed and for each time the player is
destroyed.  The score sheet displays each player's score and the number
of wins and losses.</para>

<para>A team's score is calculated differently depending on the game style.
In the capture-the-flag style, the team score is the number of enemy
flags captured minus the number of times the team's flag was captured.
Capturing your own flag (by taking it onto an enemy base) counts as a
loss.  In the free-for-all style, the team score is sum of the wins of
all the players on the team minus the sum of the losses of all the
players on the team. In the rabbit-hunt style, scoring is similar to
free-for-all.</para>

<para>The score sheet also lists the number times you have destroyed or been
destroyed by each other player under the <emphasis remap='I'>Kills</emphasis> heading.  This
lets you compare your one-on-one performance against other players.</para>
</refsect2>

<refsect2 id='teleporters'><title>Teleporters</title>
<para>The server can be configured to place teleporters in the game.
A teleporter is a tall black transparent object that instantaneously
moves any object (tanks and shots) passing through it to some other
teleporter.  The teleporter connections are fixed for the entire
game.  In the capture-the-flag style the connections are always
the same.  In the free-for-all style the connections are random and
reversable (going back through where you come out puts you back where
you started).</para>

<para>Each side of a teleporter teleports independently of the other
side.  However, it's possible for each side to go to the other.
This is a thru-teleporter and it's almost as if it weren't there.
It's also possible for a side to teleport to itself.  This is a
reverse-teleporter.  Shooting at a reverse teleporter is likely
to be self-destructive.  Shooting a laser at a reverse teleporter
is invariably fatal.</para>
</refsect2>

<refsect2 id='radar'><title>Radar</title>
<para>The radar is displayed on the left side of the control panel.  It
provides a satellite view of the game.  Buildings and the outer wall
are light blue.  Team bases are outlined squares in the team colors.
Teleporters are short yellow lines.  Tanks are dots the in the tank's
team color, except for rogues which are yellow.  The size of a tank's
dot is a rough indication of the tank's altitude: higher tanks have
larger dots.  Flags are small crosses.  Team flags have the team color
while super-flags are white.  Shots are small white dots (except laser
beams which are line segments and shock waves which are circles).</para>

<para>The tank always appears in the center of the radar and the radar
display rotates with the tank so that forward is always up.  There's
a small tick mark indicating forward.  The left and right extremes
of the current view are represented by a yellow V who's tip is at
the center of the radar.  North is indicated by the letter N.</para>
</refsect2>

<refsect2 id='heads_up_display'><title>Heads Up Display</title>
<para>The heads-up-display, or HUD, has several displays.  First, there are
two boxes in the center of the view.  As explained above, these
delimit the ranges for the mouse.  These boxes are yellow when you
have no flag.  Otherwise they take the color of the flag you're
holding (white for superflags).</para>

<para>Above the larger box is a heading tape showing your current heading.
North is 0, east is 90, etc.  If jumping is allowed, an altitude tape
appears to the right of the larger box.</para>

<para>Small colored diamonds or arrows may appear on the heading tape.  An
arrow pointing left means that a particular flag is to your left, an
arrow pointing right means that the flag is to your right, and a
diamond indicates the heading to the flag by its position on the
heading tape.  In capture-the-flag mode a marker in your team's color
is always present, showing you the direction to your team's flag.  A
yellow marker shows the way to the antidote flag (when you have a bad
flag and antidote flags are enabled).</para>

<para>At the top of the HUD are several text readouts.  At the very top
on the left is your callsign and score, in your team's color.  At the
very top on the right is the name of the flag you're holding (or nothing
if you have no flag).  In the center at the top is your current status:
ready, dead, sealed, zoned, or reloading. If you have a bad flag and
shaking time is enabled and your status is ready, the status displays
how much time is left before the bad flag is shaken.  When reloading,
the time until you're reloaded is displayed.  A tank is sealed when it
has the oscillation overthruster flag and any part of the tank is inside
a building. A tank is zoned when it has the phantom zone flag and has
passed through a teleporter.  When there's a time limit on the game,
the time left in the game is displayed to the left of the status.</para>
</refsect2>

<refsect2 id='flags'><title>Flags</title>
<para>Team flags are supplied by the server in the capture-the-flag
style game.  While at least one player is on a team, that team's
flag is in the game.  When captured, the flag is returned to
the team's base.  If the flag is dropped in a Bad Place, it is
moved to a safety position.  Bad Places are:  on top of a building
or on an enemy team base.  The flag can be dropped on a team base
only by a player from a third team;  for example, when a blue
player drops the red flag on the green base.</para>

<para>A team flag is captured when a tank takes an enemy flag onto its
base or when a tank takes its flag onto an enemy base (even if
there's no one playing on that team).  You must be on the ground
to capture a flag.</para>

<para>The server can be configured to supply a fixed or random set of
super-flags.  These flags are white and come in many flavors.
However, you cannot tell what a super-flag is until it's picked
up.  There are two broad categories of super-flags:  good and bad.
Good super-flags may be dropped and will remain for up to 4 possessions.
Bad super-flags are sticky -- in general, they cannot be dropped.  The
server may provide a yellow antidote flag.  Driving over it will release
the bad flag.  The server may also allow a timeout and/or a number of wins
to shake the flag.  Scoring the required number of wins, surviving the
required amount of time or being destroyed will automatically drop the flag.
Bad flags disappear after the first possession.</para>

<para>Here is a brief description of each good superflag with the flag's code
in parentheses:</para>
<!-- .RS -->
<variablelist remap=".TP">
<varlistentry>
<term><emphasis remap='B'>High Speed (V)</emphasis></term>
<listitem>
<para>Boosts top speed by 50%.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Quick Turn (A)</emphasis></term>
<listitem>
<para>Boosts turn rate by 50%.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Oscillation Overthruster (OO)</emphasis></term>
<listitem>
<para>Let's the tank go through buildings.  You cannot back up in or into
a building, nor can you shoot while inside.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Rapid Fire (F)</emphasis></term>
<listitem>
<para>Increases shot speed and decreases range and reload delay.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Machine Gun (MG)</emphasis></term>
<listitem>
<para>Increases shot speed and dramatically decreases range and reload delay.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Guided Missile (GM)</emphasis></term>
<listitem>
<para>Shots guide themselves when locked on.  The missile can be retargeted at
any time during its flight (with the right mouse button).  This allows
the player some control over the missile's steering.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Laser (L)</emphasis></term>
<listitem>
<para>Shoots a laser, with effectively infinite speed and range.  Just point
and shoot.  The binoculars are handy for lining up distant targets.
The downside (you knew it was coming) is that the reload time is doubled.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Ricochet (R)</emphasis></term>
<listitem>
<para>Shots reflect off walls.  It is exceptionally easy to kill yourself with
this flag.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Super Bullet (SB)</emphasis></term>
<listitem>
<para>Shots can go through buildings (possibly destroying a tank with the
oscillation overthruster flag) and can also destroy (phantom) zoned
tanks.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Stealth (ST)</emphasis></term>
<listitem>
<para>Tank becomes invisible on radar but is still visible out-the-window.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Cloaking (CL)</emphasis></term>
<listitem>
<para>Tank becomes invisible out-the-window but is still visible on radar.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Invisible Bullet (IB)</emphasis></term>
<listitem>
<para>Shots are invisible on radar (except your own).  They are visible
out-the-window.  Sort of stealth for shots.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Tiny (T)</emphasis></term>
<listitem>
<para>Tank becomes much smaller and harder to hit.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Narrow (N)</emphasis></term>
<listitem>
<para>Tank becomes paper thin.  It's very hard (but not impossible) to hit
a narrow tank from the front or back.  However, the tank is as
long as usual so hitting it from the side has normal difficulty.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Shield (SH)</emphasis></term>
<listitem>
<para>Getting shot while in possession of this flag simply drops the flag
(instead of destroying the tank).  Since the flag may not disappear
you may want to wait around for it to fall to the ground so you can
grab it again, but, be warned, the shield flag flies for an extra
long time (longer than the normal reload time).</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Steamroller (SR)</emphasis></term>
<listitem>
<para>Tank can destroy other tanks by driving over them (but you must get
quite close).</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Shock Wave (SW)</emphasis></term>
<listitem>
<para>Tank doesn't fire shells.  Instead it sends out a shock wave in all
directions.  Any tank caught in the wave is destroyed (including
tanks on or in buildings).</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Phantom Zone (PZ)</emphasis></term>
<listitem>
<para>Driving through a teleporter phantom zones the tank.  A zoned tank
cannot shoot, but can drive through buildings and cannot be destroyed
except by a Super Bullet or a Shock Wave (or if the team's flag
is captured).</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Genocide (G)</emphasis></term>
<listitem>
<para>Destroying any tank on a team destroys every player on that team.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Jumping (JP)</emphasis></term>
<listitem>
<para>Allows the tank to jump.  You cannot steer while in the air.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Identify (ID)</emphasis></term>
<listitem>
<para>Displays the identity of the closest flag in the vicinity.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Masquerade (MQ)</emphasis></term>
<listitem>
<para>You tank looks like a teammate when viewed out of the window.
Bullets, radar and targeting reveal your true identity.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Burrow {BU}</emphasis></term>
<listitem>
<para>You tank burrows into the ground up to your muzzle, making you
impervious to normal shots, as they sail above you. However
your tank controls are sluggish, and anyone, no matter what flag
they have, can crush you like.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Seer (SE)</emphasis></term>
<listitem>
<para>See Stealthed, Cloaked and Masqueraded tanks as normal.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Thief (TH)</emphasis></term>
<listitem>
<para>Tank is small and fast, when you shoot an opponent, he is not killed,
but instead, you steal his flag.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Useless (US)</emphasis></term>
<listitem>
<para>It's useless!</para>
<!-- .RE -->
</listitem>
</varlistentry>
</variablelist> <!-- .TP -->

<para>A brief description of each bad superflag with the flag's code
in parentheses:</para>
<!-- .RS -->
<variablelist remap=".TP">
<varlistentry>
<term><emphasis remap='B'>Colorblindness (CB)</emphasis></term>
<listitem>
<para>Prevents tank from seeing any team information about other tanks.
You have to be careful to avoid shooting teammates.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Obesity (O)</emphasis></term>
<listitem>
<para>The tank becomes very large and easy to hit.  It's so big that it
can't fit through teleporters.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Left Turn Only (&lt;-)</emphasis></term>
<listitem>
<para>Prevents the tank from turning right.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Right Turn Only (-&gt;)</emphasis></term>
<listitem>
<para>Prevents the tank from turning left.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Momentum (M)</emphasis></term>
<listitem>
<para>Gives the tank a lot of inertia.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Blindness (B)</emphasis></term>
<listitem>
<para>Blanks the out-the-window view.  The radar still works.  It is
effectively impossible to detect any tank with Stealth;  shooting
a Stealth with Blindness is the stuff legends are made of.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Jamming (JM)</emphasis></term>
<listitem>
<para>Disables the radar but you can still see.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><emphasis remap='B'>Wide Angle (WA)</emphasis></term>
<listitem>
<para>Gives the tank a fish eye lens that's rather disorienting.</para>
</listitem>
</varlistentry>
</variablelist> <!-- .TP -->
</refsect2>

<refsect2 id='observing'><title>Observing</title>
<para>If a server is full or if you just want to watch a battle without interfering
in it, you can use the observer mode. To join a server as an observer, select
<emphasis remap='I'>Observer</emphasis>
as your tank's team. The maximum number of observers can be
restricted by the server admin, so you might still not be able to join a
full server.</para>

<para>When in observer mode, you can freely roam the world. Using the
arrow keys you can rotate the camera in every direction. Holding shift and
using the arrow keys moves the camera left, right, forward or back. Pressing
the up or down arrow while holding the the
<emphasis remap='I'>ALT</emphasis>
key will change the camera's altitude. The
<emphasis remap='I'>F9</emphasis>
and
<emphasis remap='I'>F10</emphasis>
keys change the camera's focal lengths, giving a zoom effect. The
<emphasis remap='I'>F11</emphasis>
key will reset the zoom. Pressing
<emphasis remap='I'>l</emphasis>
lets you toggle the display of tank labels.</para>

<para>Repeatedly pressing
<emphasis remap='I'>F8</emphasis>
cycles through different roaming modes: <emphasis remap='I'>free</emphasis>, <emphasis remap='I'>tracking</emphasis>,
<emphasis remap='I'>following</emphasis>, <emphasis remap='I'>first person (driving with)</emphasis> and <emphasis remap='I'>tracking team flag</emphasis>.
In <emphasis remap='I'>tracking</emphasis> mode, the camera will automatically look at a tank. You can cycle through
available tanks with the
<emphasis remap='I'>F6</emphasis>
and
<emphasis remap='I'>F7</emphasis>
keys. In <emphasis remap='I'>follow</emphasis> mode, the camera is positioned right behind the
targeted tank, whereas you actually look from within the tank when using
<emphasis remap='I'>first person</emphasis> mode. The last mode, <emphasis remap='I'>track team flag</emphasis> is only available in capture-the-flag games and
will track the team flags. Again, use
<emphasis remap='I'>F6</emphasis>
and
<emphasis remap='I'>F7</emphasis>
to choose which flag to track. One special option that can be used with <emphasis remap='I'>follow</emphasis>, <emphasis remap='I'>tracking</emphasis>,
and <emphasis remap='I'>first person</emphasis> modes is that you can choose to do it with the winning tank.
This is selected by cycling through the tanks until you see the winner option. In this mode,
you will always be engaged with whomever has the best score (and is alive).
The default is <emphasis remap='I'>drive with winner</emphasis> mode.</para>
<!-- .RE -->
</refsect2>
</refsect1>

<refsect1 id='user_commands'><title>User Commands</title>
<!-- .RS -->
<para>The following commands can be executed by sending a message to all and using these strings as the message</para>
<variablelist remap=".TP">
<varlistentry>
<term>CLIENTQUERY</term>
<listitem>
<para>Requests that all users send you there version information</para>
</listitem>
</varlistentry>
<varlistentry>
<term>SILENCE <emphasis remap='I'>playerName</emphasis></term>
<listitem>
<para>Does not display any message coming from player with <emphasis remap='I'>playerName</emphasis> name</para>
</listitem>
</varlistentry>
<varlistentry>
<term>UNSILENCE <emphasis remap='I'>playerName</emphasis></term>
<listitem>
<para>Reshow messages coming from player with <emphasis remap='I'>playerName</emphasis> name</para>
<!-- .RE -->
</listitem>
</varlistentry>
</variablelist> <!-- .TP -->
</refsect1>

<refsect1 id='files'><title>FILES</title>
<!-- .RS -->
<variablelist remap=".TP">
<varlistentry>
<term><filename>$(HOME)/.bzflag</filename></term>
<listitem>
<para>Stores options between game sessions.  Used when HOST is not defined.</para>
</listitem>
</varlistentry>
<varlistentry>
<term><filename>$(HOME)/.bzflag.$(HOST)</filename></term>
<listitem>
<para>Stores options between game sessions.  Used when HOST is defined.</para>
<!-- .RE -->
</listitem>
</varlistentry>
</variablelist> <!-- .TP -->
</refsect1>

<refsect1 id='see_also'><title>SEE ALSO</title>
<para><citerefentry><refentrytitle>bzfls</refentrytitle><manvolnum>6</manvolnum></citerefentry>, <citerefentry><refentrytitle>bzfs</refentrytitle><manvolnum>6</manvolnum></citerefentry></para>
</refsect1>
</refentry>

