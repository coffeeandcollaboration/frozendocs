#####Primary Actor, Drone Vehicle#####

1. Locate Target
2. Determine Target Non-Friendly
3. Fire At Target
4. Target Destroyed - Search for New Target
5. All Enemies Destroyed = Win Game

###Secondary Actor###

1. Target Vehicle
2. Perform Evasive Manuevers
3. Fire At Target
4. Vehicle Destroyed = Win Game